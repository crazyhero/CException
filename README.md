#CException

1. 可以让C像C++一样抛出捕获异常。
2. 支持系统错误消息打印。
3. 支持多线程使用异常模块。
4. 支持自定义全局的异常，并能抛出或捕获。
5. 支持特殊异常处理，可以越过多个异常栈，直达某个指定的异常处理处。

```c
int test_exception()
{
	TRY
	{
		TRY
		{
			TRY
			{
				TRY
				{
					TRY
					{
						TRY
						{
							SKIP_RAISE(EXCEPT_ASSERT);
							// RAISE(EXCEPT_ASSERT);
						}
						FINALLY
						{ printf("normal stack\n"); }
						END;
					}
					CATCH
					{ printf("normal stack\n"); }
					END;
				}
				CATCH
				{ printf("normal stack\n"); }
				END;
			}
			CATCH
			{ printf("normal stack\n"); }
			END;
		}
		CATCH
		{ printf("normal stack\n"); }
		END;
	}
	CATCH
	{ printf("top stack\n"); }
	END;
	return EXIT_SUCCESS;
}
```