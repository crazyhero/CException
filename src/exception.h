//
//  exception.h
//
//
//  Created by 周凯 on 15/8/28.
//
//

#ifndef __exception__
#define __exception__

#include "except.h"

__BEGIN_DECLS
/* -------              */
/** 系统调用错误*/
#define EXCEPT_SYS_CODE         (0xffffffff)
extern const ExceptT EXCEPT_SYS;
/** 表达式断言失败*/
#define EXCEPT_ASSERT_CODE      (0xfffffffe)
extern const ExceptT EXCEPT_ASSERT;

/* -------              */
__END_DECLS
#endif	/* defined(__exception__) */

