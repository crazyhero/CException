//
//  exception.c
//
//
//  Created by 周凯 on 15/8/28.
//
//

#include "exception.h"

const ExceptT EXCEPT_SYS = {
	"System call error.",
	EXCEPT_SYS_CODE
};

const ExceptT EXCEPT_ASSERT = {
	"Assertion failed, Invalid arguments or conditions.",
	EXCEPT_ASSERT_CODE
};

